import * as mat4 from 'gl-mat4'
import * as vec4 from 'gl-vec4'
import {toBeDeepCloseTo} from './jest-matcher-deep-close-to'
expect.extend({toBeDeepCloseTo})

it('identity', () => {
  expect(mat4.create()).toEqual(new Float32Array([
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1,
    ]))
})

it('scales point', () => {
  const point = vec4.fromValues(0.5, 2, 3, 1)
  const transform = mat4.fromScaling(
    mat4.create(), 
    vec4.fromValues(10, 20, 30, 0)
  )
  expect(
    vec4.transformMat4(point, point, transform)
  ).toEqual(
    vec4.fromValues(5, 40, 90, 1)
  )
})

it('translates point', () => {
  const point = vec4.fromValues(0.5, 2, 3, 1)
  const transform = mat4.fromTranslation(
    mat4.create(), 
    vec4.fromValues(1, 3, 5, 0)
  )
  expect(
    vec4.transformMat4(point, point, transform)
  ).toEqual(
    vec4.fromValues(1.5, 5, 8, 1)
  )
})

it('doesn\'t translate vector', () => {
  const vec = vec4.fromValues(0.5, 2, 3, 0)
  const transform = mat4.fromTranslation(
    mat4.create(), 
    vec4.fromValues(1, 3, 5, 0)
  )
  expect(
    vec4.transformMat4(vec, vec, transform)
  ).toEqual(
    vec4.fromValues(0.5, 2, 3, 0)
  )
})

it('combines transformations', () => {
  const point = vec4.fromValues(0.5, 2, 3, 1)

  const translation = mat4.fromTranslation(
    mat4.create(), 
    vec4.fromValues(1, 3, 5, 0)
  )

  const scale = mat4.fromScaling(
    mat4.create(), 
    vec4.fromValues(2, 3, 4, 0)
  )

  const combinedTransform = mat4.multiply(
    mat4.create(), 
    scale,
    translation
  )

  expect(
    vec4.transformMat4(point, point, combinedTransform)
  ).toEqual(
    vec4.fromValues(3, 15, 32, 1)
  )
})

it('combining transformations, order is important', () => {
  const point = vec4.fromValues(0.5, 2, 3, 1)

  const translation = mat4.fromTranslation(
    mat4.create(), 
    vec4.fromValues(1, 3, 5, 0)
  )

  const scale = mat4.fromScaling(
    mat4.create(), 
    vec4.fromValues(2, 3, 4, 0)
  )

  const combinedTransform1 = mat4.multiply(
    mat4.create(), 
    scale,
    translation
  )
  const combinedTransform2 = mat4.multiply(
    mat4.create(), 
    translation,
    scale
  )

  expect(
    vec4.transformMat4(vec4.create(), point, combinedTransform1)
  ).not.toEqual(
    vec4.transformMat4(vec4.create(), point, combinedTransform2)
  )
})

it('matrix rotations: applying order is from last to first', () => {
  let point;
  let transform;

  point = vec4.fromValues(0, 0, 1, 1)
  transform = mat4.create();
  transform = mat4.rotate(
    transform, 
    transform,
    Math.PI / 2,
    vec4.fromValues(0, 1, 0, 0)
  )

  expect(
    vec4.transformMat4(vec4.create(), point, transform)
  ).toBeDeepCloseTo(
    vec4.fromValues(1, 0, 0, 1)
  )

  point = vec4.fromValues(0, 0, 1, 1)
  transform = mat4.create();
  transform = mat4.rotate(
    transform, 
    transform,
    Math.PI / 2,
    vec4.fromValues(0, 0, -1, 0)
  )
  transform = mat4.rotate(
    transform, 
    transform,
    Math.PI / 2,
    vec4.fromValues(1, 0, 0, 0)
  )

  expect(
    vec4.transformMat4(vec4.create(), point, transform)
  ).toBeDeepCloseTo(
    vec4.fromValues(-1, 0, 0, 1)
  )

  point = vec4.fromValues(0, 0, 1, 1)
  transform = mat4.create();
  transform = mat4.rotate(
    transform, 
    transform,
    Math.PI / 2,
    vec4.fromValues(0, -1, 0, 0)
  )
  transform = mat4.rotate(
    transform, 
    transform,
    Math.PI / 2,
    vec4.fromValues(0, 0, -1, 0)
  )
  transform = mat4.rotate(
    transform, 
    transform,
    Math.PI / 2,
    vec4.fromValues(1, 0, 0, 0)
  )

  expect(
    vec4.transformMat4(vec4.create(), point, transform)
  ).toBeDeepCloseTo(
    vec4.fromValues(0, 0, -1, 1)
  )
})
