import CANNON from 'cannon'

it('sphere trimesh test', () => {
  var vertices = [
            -1, 0, 1,
            -1, 0, -1,
            1, 0, -1,
            1, 0, 1,
        ];
  var indices = [
            0, 1, 2,
            2, 3, 0,
        ];

  const rotation = new CANNON.Quaternion()
  rotation.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), 0)
  rotation.normalize()

  const world = new CANNON.World()
  const narrowPhase = new CANNON.Narrowphase()

  const trimeshBody = new CANNON.Body({
    shape: new CANNON.Trimesh(vertices, indices),
  })

  const sphereBody = new CANNON.Body({
    position: new CANNON.Vec3(0, 0.999, 0),
    shape: new CANNON.Sphere(1),
  })

  const result = [];
  world.narrowphase.getContacts(
    [trimeshBody], 
    [sphereBody], 
    world, 
    result, 
    [], 
    [], 
    []
    );

  expect(result.length).toBeGreaterThan(0)
})