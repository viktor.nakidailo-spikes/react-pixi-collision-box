import React from 'react';
import ReactDOM from 'react-dom';
import { Stage, Graphics, AppConsumer } from '@inlet/react-pixi'

import * as mat4 from 'gl-mat4'
import * as vec4 from 'gl-vec4'

import CANNON from 'cannon'

function toPixiViewport(transform, scale) {
  transform = mat4.translate(
    transform, 
    transform, 
    vec4.fromValues(400, 300, 0, 0)
  )
  transform = mat4.scale(
    transform, 
    transform,
    vec4.fromValues(scale, -scale, scale, 0)
  )
}

function getWorldTransform() {
  let transform = mat4.create();
  toPixiViewport(transform, 150)

  transform = mat4.rotate(
    transform, 
    transform,
    Math.PI / 8,
    vec4.fromValues(1, 0, 0, 0)
  )
  transform = mat4.rotate(
    transform, 
    transform,
    Math.PI / 8,
    vec4.fromValues(0, 1, 0, 0)
  )
  return transform;
}

function getTransformedVertex(origVertex) {
  return vec4.transformMat4(vec4.create(), origVertex, getWorldTransform())
}

function getTransformedVertexes(origVertexes) {
  return origVertexes.map(v => getTransformedVertex(v))
}

class Scene extends React.Component {

  box = [
    vec4.fromValues(-1, 1, -1, 1),
    vec4.fromValues(1, 1, -1, 1),
    vec4.fromValues(1, 1, 1, 1),
    vec4.fromValues(-1, 1, 1, 1),

    vec4.fromValues(-1, -1, -1, 1),
    vec4.fromValues(1, -1, -1, 1),
    vec4.fromValues(1, -1, 1, 1),
    vec4.fromValues(-1, -1, 1, 1),
  ]

  boxToCannonVertices() {
    let res = []
    for (let floatArr of this.box) {
      res.push(floatArr[0], floatArr[1], floatArr[2])
    }
    return res
  }

  triIndices = [
    0, 1, 4,
    4, 1, 5,

    1, 2, 5, 
    5, 2, 6,

    2, 3, 6,
    6, 3, 7,

    3, 0, 7,
    7, 0, 4,

    //floor
    4, 5, 7,
    7, 5, 6,
  ]

  backLines = [
    [0, 1],
    [1, 2],

    [0, 4],
    [1, 5],

    [4, 5],
    [5, 6],
  ]

  frontLines = [
    [2, 3],
    [3, 0],

    [2, 6],
    [3, 7],

    [6, 7],
    [7, 4],
  ]

  state = { 
    center: vec4.fromValues(0, 1, 0, 1),
    velocity: vec4.fromValues(0, 0, 0, 0),
  }

  gForce = vec4.fromValues(0, -0.01, 0, 0)
  radius = vec4.fromValues(0.4, 0, 0, 0)

  constructor() {
    super()

    const rotation = new CANNON.Quaternion()
    rotation.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), 0)
    rotation.normalize()

    this.world = new CANNON.World()

    this.trimeshBody = new CANNON.Body({
      shape: new CANNON.Trimesh(this.boxToCannonVertices(), this.triIndices),
    })

    this.sphereBody = new CANNON.Body({
      position: new CANNON.Vec3(
        this.state.center[0],
        this.state.center[1],
        this.state.center[2],
      ),
      shape: new CANNON.Sphere(vec4.length(this.radius)),
    })
  }

  frame() {
    this.sphereBody.position.set(
      this.state.center[0],
      this.state.center[1],
      this.state.center[2],
    )

    const result = [];
    this.world.narrowphase.getContacts(
      [this.trimeshBody], 
      [this.sphereBody], 
      this.world, 
      result, 
      [], 
      [], 
      []
      );

    let velocity = this.state.velocity
    if (result.length > 0) {
      if (vec4.dot(velocity, this.gForce) > 0) {
        velocity[1] = -velocity[1]
      }
    }
    velocity = vec4.add(vec4.create(), velocity, this.gForce)
    if (result.length > 0) {
      if (vec4.dot(velocity, this.gForce) > 0) {
        velocity[1] = 0
      }
    }
    
    let center = vec4.add(vec4.create(), this.state.center, velocity)

    this.setState(state => ({
      velocity: velocity, 
      center: center,
    }))
  }
  componentDidMount() {
    this.props.app.ticker.add(this.tick)
  }

  componentWillUnmount() {
    this.props.app.ticker.remove(this.tick)
  }

  tick = (delta) => {
    this.frame()
  }


  drawLines(g, lines) {
    const transformedBoxPoints = getTransformedVertexes(this.box)
    for (const line of lines) {
      const pos1 = transformedBoxPoints[line[0]]
      const pos2 = transformedBoxPoints[line[1]]

      g.moveTo(pos1[0], pos1[1])
      g.lineTo(pos2[0], pos2[1])
    }
  }

  render() {
    return (
      <React.Fragment>
        <Graphics
          draw={g => {
            g.clear()
            g.lineStyle(4, 0xff4400, 1)
            this.drawLines(g, this.backLines)
          }}
        />
        <Graphics
          draw={g => {
            const center = getTransformedVertex(this.state.center)
            const radius = getTransformedVertex(this.radius)

            g.clear()
            g.lineStyle(4, 0xffd900, 1)
            g.beginFill(0xFFFF0B, 1);
            g.drawCircle(center[0], center[1], vec4.length(radius));
            g.endFill();
          }}
        />
        <Graphics
          draw={g => {
            g.clear()
            g.lineStyle(4, 0xffd900, 1)
            this.drawLines(g, this.frontLines)
          }}
        />
      </React.Fragment>
    )
  }
}

const App = () => (
  <Stage options={{ backgroundColor: 0x012b30 }}>
    <AppConsumer>
      {app => <Scene app={app}/>}
    </AppConsumer>
  </Stage>
)

ReactDOM.render(<App />, document.getElementById('root'));
